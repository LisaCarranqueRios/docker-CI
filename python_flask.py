# -*- coding: utf-8 -*- 
from flask import Flask, request
import pprint
import redis
import time

app = Flask(__name__)

#connexion redis via python : la base redis est installée et accédée via cet host et configurée avec ce port
r = redis.Redis(host='0.0.0.0',port=6379)

@app.route("/distance", methods=['GET','POST']) 
def esp():
   if request.method == 'POST': 

   
      distance = request.form["valeur"]
      #utilisation d'un timestamp pour horodater les valeurs mesurées
      ts = time.time() 

   #stockage de uplet (cle,valeur)
      r.set(ts,distance)  
      return distance

   elif request.method == 'GET':
    #recuperation des valeurs mesurees associées aux cles
      tab = ''
      for key in r.scan_iter():
         tab += ("clef : " + str(key) + " // valeur : " + str(r.get(key)) + " || ")
      return str(tab) 

if __name__ == '__main__':
   app.run(debug=True)
