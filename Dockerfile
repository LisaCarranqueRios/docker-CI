FROM python:3.7 
COPY . /home/zenika/Bureau/docker-CI
WORKDIR /home/zenika/Bureau/docker-CI
# install system-wide deps for python
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY ./python_flask.py /
CMD python ./python_flask.py
